#! /bin/sh

#------------------#
#	@Author: Aravinda
#	Initial Release: Sept 12, 2016
#------------------#

notify=false
charge=`upower -i $(upower -e | grep BAT)  | awk '/perc.+/ { print $2 }'`
chargev=${charge%\%}
case $charge in
	0[0-9]%|1[0-5]%)
		notify-send "$charge % you better save ur work before i die"
		paplay notify.ogg;
		;;
	1[6-9]%|2[0-4]%)
		notify-send "$charge %, plug the charger now...!"
		paplay notify.ogg;
		;;
	2[5-9]%)
		notify-send  "Hurry up...!, go get your charger, Your charge is $charge"
		paplay notify.ogg;
		;;
	3[0-9]%|4[0-9])
		notify-send "Okay, you have some charge left"
		paplay notify.ogg;
		;;
	100%|9[0-9]%)
		notify-send "Sit down and relax, you have enough ($charge) juice"
		paplay notify.ogg;
		;;
	*)
		notify-send "Dont worry,  you can survive for few hours";
		paplay notify.ogg;
		;;
esac
